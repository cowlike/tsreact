export interface StoreState {
    name: string;
    languageName: string;
    enthusiasmLevel: number;
}
